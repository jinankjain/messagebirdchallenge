package main

import (
	"github.com/jinankjain/MessageBirdChallenge/cmd"
)

func main() {
	cmd.Execute()
}
