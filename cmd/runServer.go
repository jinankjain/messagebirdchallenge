package cmd

import (
	apiserver "github.com/jinankjain/MessageBirdChallenge/apiserver"
	"github.com/spf13/cobra"
)

var runServerCmd = &cobra.Command{
	Use:   "run",
	Short: "Command to run server",
	Run: func(cmd *cobra.Command, args []string) {
		apiserver.RunServer(rootConfig.port, rootConfig.debug)
	},
}

func init() {
	RootCmd.AddCommand(runServerCmd)
}
