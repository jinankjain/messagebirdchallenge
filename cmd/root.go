package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootConfig rootFlags

type rootFlags struct {
	port  int
	debug bool
}

var RootCmd = &cobra.Command{
	Use:   "messagebird",
	Short: "REST API for messagebird",
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	RootCmd.PersistentFlags().IntVarP(&rootConfig.port, "port", "p", 8000, "port (default 8000)")
	RootCmd.PersistentFlags().BoolVarP(&rootConfig.debug, "debug", "d", false, "default debug mode false")
}
