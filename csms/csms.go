package csms

import (
	"fmt"
	"math"
	"math/rand"
)

type CSMS struct {
	Message string
	Udh     string
}

// Assuming that message contains 7-bit GSM words
// Warning this implementation does not take care of 8-bit Unicode characters
func ConvertMessageToCSMS(message string) (csms []CSMS) {
	messageLength := len(message)
	chunkSize := 153
	numberOfChunks := int(math.Ceil(float64(messageLength) / float64(chunkSize)))
	offset := 0
	messageIdentifier := fmt.Sprintf("%02X", rand.Intn(256))
	udhPrefix := "050003" + messageIdentifier + fmt.Sprintf("%02X", numberOfChunks)
	remLength := messageLength

	for i := 1; i <= numberOfChunks; i++ {
		sizeSent := min(chunkSize, remLength)
		csms = append(csms, CSMS{
			Message: message[offset : offset+sizeSent],
			Udh:     udhPrefix + fmt.Sprintf("%02X", i),
		})
		offset += sizeSent
		remLength -= sizeSent
	}

	return
}
