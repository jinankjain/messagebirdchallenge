package csms

import (
	"math/rand"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func generateRandomMessage(n int) (message []string) {
	chunkSize := 153
	for i := 0; n > 0; i++ {
		currSize := min(n, chunkSize)
		message = append(message, randStringRunes(currSize))
		n -= currSize
	}
	return
}

func TestMessageSize2Leftover(t *testing.T) {
	message := generateRandomMessage(303)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 2 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}
}

func TestMessageSize2Exact(t *testing.T) {
	message := generateRandomMessage(306)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 2 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}

}

func TestMessageSize100Leftover(t *testing.T) {
	message := generateRandomMessage(15298)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 100 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}
}

func TestMessageSize100Exact(t *testing.T) {
	message := generateRandomMessage(15300)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 100 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}

}

func TestMessageSize255Leftover(t *testing.T) {
	message := generateRandomMessage(39013)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 255 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}
}

func TestMessageSize255Exact(t *testing.T) {
	message := generateRandomMessage(39015)

	completeMessage := ""
	for _, m := range message {
		completeMessage += m
	}

	csms := ConvertMessageToCSMS(completeMessage)

	if len(csms) != 255 {
		t.Error("Number of chunks were not equal")
	}

	for i, sms := range csms {
		if message[i] != sms.Message {
			t.Error("Message %d did not match", i)
		}
	}

}
