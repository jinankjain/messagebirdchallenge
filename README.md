# MessageBirdChallenge
Software engineer interview assignment

## Task Description

An `API` to accept `SMS` submitted via a `POST` request containing a JSON object in the format specified below

```json
{
   "recipient":31612345678,
   "originator":"MessageBird",
   "message":"This is a test message."
}
```

## Usage

```raw
REST API for messagebird

Usage:
  messagebird [command]

Available Commands:
  help        Help about any command
  run         Command to run server

Flags:
  -d, --debug      default debug mode false
  -h, --help       help for messagebird
  -p, --port int   port (default 8000) (default 8000)

Use "messagebird [command] --help" for more information about a command.
```

## Running Application and Test

In order to run you need to export MESSAGEBIRD\_ACCESS\_KEY as an environment variable

#### Run test

```
make test
```

#### Build docker container

```
make docker-build
```
