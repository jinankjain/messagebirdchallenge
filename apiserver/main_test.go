package apiserver

import (
	logrus "github.com/sirupsen/logrus"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var fakeServer *httptest.Server

func startFakeServer() {
	logger = logrus.New()
	fakeServer = httptest.NewServer(http.HandlerFunc(messagesHandler))
	newClient()
}

func stopFakeServer() {
	fakeServer.Close()
}

func TestMain(m *testing.M) {
	startFakeServer()
	exitCode := m.Run()
	stopFakeServer()
	os.Exit(exitCode)
}
