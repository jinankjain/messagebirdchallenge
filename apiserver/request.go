package apiserver

type Request struct {
	Originator string      `json: "originator"`
	Message    string      `json: "message"`
	Recipients interface{} `json: "recipients"`
}
