package apiserver

import (
	logrus "github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"strconv"
)

func RunServer(port int, debug bool) {
	router := newRouter()
	newClient()
	debugLog = debug

	logger = logrus.New()
	w := logger.Writer()
	defer w.Close()

	server := &http.Server{
		Addr:     ":" + strconv.Itoa(port),
		Handler:  router,
		ErrorLog: log.New(w, "", 0),
	}

	logger.Info("Starting server at http://localhost", server.Addr)
	err := server.ListenAndServe()

	// If server launch failed kill the process
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
