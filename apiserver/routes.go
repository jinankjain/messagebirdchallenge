package apiserver

import (
	"net/http"
)

type Route struct {
	pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func newRouter() (mux *http.ServeMux) {
	mux = http.NewServeMux()
	for _, route := range routes {
		mux.HandleFunc(route.pattern, route.HandlerFunc)
	}
	return
}

var routes = Routes{
	Route{
		"/messages",
		messagesHandler,
	},
}
