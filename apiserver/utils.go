package apiserver

import (
	"encoding/json"
	csms "github.com/jinankjain/MessageBirdChallenge/csms"
	messagebird "github.com/messagebird/go-rest-api"
	logrus "github.com/sirupsen/logrus"
	"os"
)

var client *messagebird.Client
var debugLog bool

const (
	maxRecipients    = 50
	maxMessageLength = 160
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

var logger *logrus.Logger

func newClient() {
	accessKey := os.Getenv("MESSAGEBIRD_ACCESS_KEY")
	client = messagebird.New(accessKey)
}

func sendMessage(request Request,
	optionalParamas messagebird.MessageParams) (messages []messagebird.Message) {
	recipients := []string{}
	switch recp := request.Recipients.(type) {
	case []interface{}:
		for _, r := range recp {
			recipients = append(recipients, r.(json.Number).String())
		}
	case json.Number:
		recipients = append(recipients, recp.String())
	}

	// MessageBird API can only support 50 recipients
	// We need to chunk recipients into size less than 50
	totalRecipients := len(recipients)
	offset := 0
	for i := 0; totalRecipients > 0; i++ {
		currSize := min(totalRecipients, maxRecipients)
		messageLength := len(request.Message)
		if messageLength > maxMessageLength {
			multipleSms := csms.ConvertMessageToCSMS(request.Message)
			for _, sms := range multipleSms {
				optionalParamas.TypeDetails = make(messagebird.TypeDetails)
				optionalParamas.TypeDetails["udh"] = sms.Udh
				optionalParamas.Type = "binary"
				response, _ := client.NewMessage(request.Originator,
					recipients[offset:offset+currSize],
					sms.Message, &optionalParamas)
				messages = append(messages, *response)
			}
		} else {
			response, _ := client.NewMessage(request.Originator,
				recipients[offset:offset+currSize],
				request.Message, &optionalParamas)
			messages = append(messages, *response)
		}
		totalRecipients -= currSize
		offset += currSize
	}

	return
}

func getMessage(messageId string) (message *messagebird.Message, err error) {
	message, err = client.Message(messageId)
	return
}
