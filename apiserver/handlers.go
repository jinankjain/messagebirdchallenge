package apiserver

import (
	"encoding/json"
	messagebird "github.com/messagebird/go-rest-api"
	"io/ioutil"
	"net/http"
	"strings"
)

func messagesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Error reading request body",
				http.StatusInternalServerError)
			break
		}
		var request Request
		var optionalParamas messagebird.MessageParams
		d := json.NewDecoder(strings.NewReader(string(body)))
		d.UseNumber()

		err = d.Decode(&struct {
			*Request
			*messagebird.MessageParams
		}{&request, &optionalParamas})

		if err != nil {
			if debugLog {
				logger.Error("Unsupported Request Format")
			}
			http.Error(w, "Unsupported request format",
				http.StatusBadRequest)
			break
		}
		if debugLog {
			logger.Info("POST REQUEST: request: ", request, " optionalParams: ", optionalParamas)
		}

		responses := sendMessage(request, optionalParamas)

		jsonbody, err := json.Marshal(responses)

		if err != nil {
			if debugLog {
				logger.Error("Error converting response to json")
			}
			http.Error(w, "Error converting response to json",
				http.StatusInternalServerError)
			break
		}

		if debugLog {
			logger.Info(string(jsonbody))
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonbody)

	case "GET":
		messageId := r.URL.Query().Get("messageId")
		if debugLog {
			logger.Info("GET REQUEST for messageId: ", messageId)
		}

		response, err := getMessage(messageId)

		if err != nil {
			if debugLog {
				logger.Error(err.Error())
			}
			http.Error(w, err.Error(), http.StatusBadRequest)
			break
		}

		jsonbody, err := json.Marshal(response)

		if err != nil {
			if debugLog {
				logger.Error("Error converting response to json")
			}
			http.Error(w, "Error converting response to json",
				http.StatusInternalServerError)
			break
		}

		if debugLog {
			logger.Info(string(jsonbody))
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonbody)
	default:
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
	}
}
