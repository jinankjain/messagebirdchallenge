package apiserver

import (
	"bytes"
	"encoding/json"
	"fmt"
	messagebird "github.com/messagebird/go-rest-api"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestPostRequest(t *testing.T) {

	url := fakeServer.URL + "/messages"
	var payload = []byte(`{"recipients":4915219326435,"originator":"MessageBird","message":"This is a test message.hhhhhhhhhhjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhqrqrqrqrqrqrqrrqrqrqrqrqrqrqrqraffasfsffsfggsfg"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var messages []messagebird.Message

	err = json.Unmarshal(body, &messages)

	if err != nil {
		t.Error(err)
	}

	if len(messages) != 2 {
		t.Error("Messages are not split properly")
	}

	for i, message := range messages {
		if message.Recipients.TotalCount != 1 {
			t.Error("Wrong number of recipients")
		}

		if message.Recipients.Items[0].Recipient != 4915219326435 {
			t.Error("Wrong recipient!")
		}

		if message.Type != "binary" {
			t.Error("Wrong message type")
		}

		if message.Originator != "MessageBird" {
			t.Error("Wrong Originator")
		}

		if message.TypeDetails["udh"].(string)[10:] != fmt.Sprintf("%02X", i+1) {
			t.Error("Wrong udh packet number")
		}

		if message.TypeDetails["udh"].(string)[8:10] != fmt.Sprintf("%02X", 2) {
			t.Error("Wrong number of total packets in udh header")
		}
	}
}

func TestPostRequestSmallMessage(t *testing.T) {

	url := fakeServer.URL + "/messages"
	var payload = []byte(`{"recipients":4915219326435,"originator":"MessageBird","message":"This is a test message.hhhhhhhhhhjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var messages []messagebird.Message

	err = json.Unmarshal(body, &messages)

	if err != nil {
		t.Error(err)
	}

	if len(messages) != 1 {
		t.Error("Messages are not split properly")
	}

	for _, message := range messages {
		if message.Recipients.TotalCount != 1 {
			t.Error("Wrong number of recipients")
		}

		if message.Recipients.Items[0].Recipient != 4915219326435 {
			t.Error("Wrong recipient!")
		}

		if message.Type != "sms" {
			t.Error("Wrong message type")
		}

		if message.Originator != "MessageBird" {
			t.Error("Wrong Originator")
		}

		if len(message.TypeDetails) != 0 {
			t.Error("Non empty message details for message < 160 chars")
		}

	}
}

func TestPostRequestInvalidPaylod(t *testing.T) {
	url := fakeServer.URL + "/messages"
	var payload = []byte(`{"recipients":4915219326435,"originators":"MessageBird","mesge":"This is a test message.hhhhhhhhhhjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	var messages []messagebird.Message

	err = json.Unmarshal(body, &messages)

	if err != nil {
		t.Error(err)
	}

	if len(messages) != 1 {
		t.Error("Messages are not split properly")
	}

	for _, message := range messages {
		if len(message.Errors) != 2 {
			t.Error("Wrong number of errors")
		}

		if message.Errors[0].Code != 9 {
			t.Error("Wrong first error code")
		}

		if message.Errors[1].Code != 10 {
			t.Error("Wrong second error code")
		}
	}
}

func TestMultiRecipientPostRequest(t *testing.T) {
	url := fakeServer.URL + "/messages"
	var payload = []byte(`{"recipients":[4915219326435, 410779810995],"originator":"MessageBird","message":"This is a test message.hhhhhhhhhhjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var messages []messagebird.Message

	err = json.Unmarshal(body, &messages)

	if err != nil {
		t.Error(err)
	}

	if len(messages) != 1 {
		t.Error("Messages are not split properly")
	}

	for _, message := range messages {
		if message.Recipients.TotalCount != 2 {
			t.Error("Wrong number of recipients")
		}

		if message.Recipients.Items[0].Recipient != 4915219326435 {
			t.Error("Wrong recipient!")
		}

		if message.Recipients.Items[1].Recipient != 410779810995 {
			t.Error("Wrong recipient!")
		}

		if message.Type != "sms" {
			t.Error("Wrong message type")
		}

		if message.Originator != "MessageBird" {
			t.Error("Wrong Originator")
		}

		if len(message.TypeDetails) != 0 {
			t.Error("Non empty message details for message < 160 chars")
		}
	}

}

func TestGetRequest(t *testing.T) {
	url := fakeServer.URL + "/messages"

	req, _ := http.NewRequest("GET", url, nil)

	values := req.URL.Query()
	values.Add("messageId", "2b656479cd604e03b7397c0e2f9e56da")

	req.URL.RawQuery = values.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var message messagebird.Message

	err = json.Unmarshal(body, &message)

	if err != nil {
		t.Error(err)
	}

	if message.Id != "2b656479cd604e03b7397c0e2f9e56da" {
		t.Error("Wrong id")
	}

	if message.Body != "This is a test message." {
		t.Error("Wrong Message Body")
	}

	if message.Recipients.TotalCount != 1 {
		t.Error("Wrong number of total recipients")
		if message.Recipients.Items[0].Recipient != 4915219326435 {
			t.Error("Wrong Recipient")
		}
	}
}

func TestInvalidMessageId(t *testing.T) {
	url := fakeServer.URL + "/messages"

	req, _ := http.NewRequest("GET", url, nil)

	values := req.URL.Query()
	values.Add("messageId", "2b656479cd604e03b7397c0e2f9e56d")

	req.URL.RawQuery = values.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var response = []byte(`The MessageBird API returned an error`)

	if !bytes.Contains(body, response) {
		t.Error("Cannot detect invalid message id", string(body))
	}
}

func TestInvalidRequestMethod(t *testing.T) {
	url := fakeServer.URL + "/messages"

	req, _ := http.NewRequest("PUT", url, nil)

	values := req.URL.Query()
	values.Add("messageId", "2b656479cd604e03b7397c0e2f9e56da")

	req.URL.RawQuery = values.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	var response = []byte(`Invalid request method`)

	if !bytes.Contains(body, response) {
		t.Error("Cannot detect invalid message id", bytes.Compare(body, response))
	}

}
